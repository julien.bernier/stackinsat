package com.stackinsat.test.controller;

import com.stackinsat.test.model.FormulairePersonneModel;
import com.stackinsat.test.model.PersonneModel;
import com.stackinsat.test.service.PersonneService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class PersonneController {

    private final PersonneService personneService;

    @GetMapping("/api/v1/personnes")
    @ApiOperation(value = "Retourne la liste des personnes triées par ordre alphabétique")
    public List<PersonneModel> list() {
        return personneService.list();
    }

    @PostMapping("/api/v1/personnes")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Création d'une nouvelle personne (seulement les personnes de moins de 150 ans peuvent être crées)")
    public PersonneModel create(@Validated @RequestBody FormulairePersonneModel form) {
        if (form.getDateNaissance().isBefore(LocalDate.now().minus(150, ChronoUnit.YEARS))) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "seule les personnes de moins de 150 ans peuvent être enregistrées");
        }
        return personneService.create(form);
    }

}
