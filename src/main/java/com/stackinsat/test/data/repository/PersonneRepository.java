package com.stackinsat.test.data.repository;

import com.stackinsat.test.data.entity.Personne;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonneRepository extends JpaRepository<Personne, Long> {

    List<Personne> findAllByOrderByNomAscPrenomAsc();

}
