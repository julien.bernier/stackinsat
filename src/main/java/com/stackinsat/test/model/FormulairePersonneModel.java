package com.stackinsat.test.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class FormulairePersonneModel {

    @NotEmpty
    private String nom;

    @NotEmpty
    private String prenom;

    @NotNull
    private LocalDate dateNaissance;

}
