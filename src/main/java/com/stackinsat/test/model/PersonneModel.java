package com.stackinsat.test.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PersonneModel {

    private Long id;

    private String nom;

    private String prenom;

    private LocalDate dateNaissance;

    private Long age;
}
