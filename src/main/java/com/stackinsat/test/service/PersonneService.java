package com.stackinsat.test.service;

import com.stackinsat.test.data.entity.Personne;
import com.stackinsat.test.data.repository.PersonneRepository;
import com.stackinsat.test.model.FormulairePersonneModel;
import com.stackinsat.test.model.PersonneModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PersonneService {

    public final PersonneRepository personneRepository;

    public List<PersonneModel> list() {
        return personneRepository.findAllByOrderByNomAscPrenomAsc().stream()
                .map(this::mapToPersonneModel)
                .collect(Collectors.toList());
    }

    public PersonneModel create(FormulairePersonneModel form) {
        Personne personne = new Personne();
        personne.setNom(form.getNom());
        personne.setPrenom(form.getPrenom());
        personne.setDateNaissance(form.getDateNaissance());
        return mapToPersonneModel(personneRepository.save(personne));
    }

    private PersonneModel mapToPersonneModel(Personne personne) {
        PersonneModel personneModel = new PersonneModel();
        personneModel.setId(personne.getId());
        personneModel.setNom(personne.getNom());
        personneModel.setPrenom(personne.getPrenom());
        personneModel.setDateNaissance(personne.getDateNaissance());
        personneModel.setAge(ChronoUnit.YEARS.between(personne.getDateNaissance(), LocalDate.now()));
        return personneModel;
    }
}
